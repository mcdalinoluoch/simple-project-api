const express = require("express");

const app = express();

const port = 4000;

const messages = [
  {
    from: "someone",
    to: "user",
    message: "This was the message",
  },
  {
    from: "user2",
    to: "someone",
    message: "How are you doing today?",
  },
];

app.get("/getmessages", (req, res) => {
  return res.json(messages);
});

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
